require "FoldIterator"

dataset = require "mnist"
train = dataset.traindataset()

options = {
    seed = 4,
    nFolds = 4,
    train = 0.6,
    validation = 0.3
}

foldIterator = FoldIterator(train.data, train.label:add(1), options) -- label from [0,9] -> [1, 10]

while( foldIterator:hasNext() ) do
	print("Fold ".. foldIterator:getFoldNumber())
	train, validation, test = foldIterator:next()
	print(train)
	print(validation)
	print(test)
end

