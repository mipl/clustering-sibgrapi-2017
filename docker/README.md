cuda-torch
==========
Ubuntu Core 14.04 + [CUDA 8.0](http://www.nvidia.com/object/cuda_home_new.html) + [cuDNN v6](https://developer.nvidia.com/cuDNN) + [Torch7](http://torch.ch/) (including iTorch) + [lua-knn](https://github.com/Saulzar/lua-knn).

Requirements
------------
- [NVIDIA Docker](https://github.com/NVIDIA/nvidia-docker)

Build
-----
Build image: ``docker build --rm -t jariasf/cuda-torch .``

Usage
-----
Use NVIDIA Docker: ``nvidia-docker run -it jariasf/cuda-torch``

Original source
---------------
Kai Arulkumaran: https://github.com/Kaixhin/dockerfiles/blob/master/cuda-torch/cuda_v8.0/README.md
