----------------------------------------------------------
-- Author: Jhosimar George Arias Figueroa
----------------------------------------------------------

require "image"
require "./data_partition/FoldIterator"
require "./utils/semisupervised"
require "./utils/grouping"
require "./utils/distances"
require "./utils/data"

-- command line options --
local cmd = torch.CmdLine()

----------------------------------------------------------
-- Input Parameters
----------------------------------------------------------
--Dataset
cmd:option('-dataSet', 'mnist', 'Dataset used')
cmd:option('-seed', -1, 'random seed')

--GPU
cmd:option('-gpu', 0, 'Using Cuda, 1 to enable')
cmd:option('-gpuID', 1, 'Set GPU id to use')

--Training
cmd:option('-batchSize', 200, 'Training batch Size')
cmd:option('-optimizer', 'adam', 'Optimizer')
cmd:option('-epoch', 100, 'Number of Epochs')
cmd:option('-decayEpoch', 30, 'Reduces the learning rate every decayEpoch')
cmd:option('-learningRate', 0.001, 'Learning rate for training')
cmd:option('-lrDecay', 0.5, 'Learning rate decay for training')

--Architecture
cmd:option('-K', 10, 'Number of clusters')
cmd:option('-inputDimension', 2, 'Dimension of the input vector into the network (e.g. 2 for height x width)')
cmd:option('-featureSize', 200, 'Size of the features learnt by the network')
cmd:option('-network', 'cnn', 'Network architecture use: cnn (Convolutional AE)') --supports only cnn
cmd:option('-nChannels', 3, 'Number of Input channels')
cmd:option('-nFilters', 16, 'Number of Convolutional Filters in first layer')

--Pretrained model
cmd:option('-pretrained', 0, 'Use pretrained model')
cmd:option('-saveModel',0, 'Save model in -fnModel file after finishing training')
cmd:option("-fnModel", "pretrained/model.t7", "Filename where model will be saved")

--Partition parameters
cmd:option('-batchSizeVal', 1000, 'Batch Size of validation data')
cmd:option('-batchSizeTest', 1000, 'Batch Size of test data')
cmd:option('-nFolds', 1, 'Number of folds')
cmd:option('-train', 0.8, 'Training partition (0.0-1.0)')
cmd:option('-validation', 0.2, 'Validation partition (0.0-1.0)')

--Gumbel parameters
cmd:option('-temperature', 1, 'Initial temperature used in gumbel-softmax (recommended 0.5-1.0)')
cmd:option('-decayTemperature', 0, 'Set 1 to decay gumbel temperature every epoch')
cmd:option('-minTemperature', 0.5, 'Minimum temperature of gumbel-softmax after annealing')
cmd:option('-decayTempRate' , 0.00693, 'Temperature decay rate every epoch')

--Loss function parameters
cmd:option("-alpha", 0.5 , "Importance of labeled data in feature loss")
cmd:option("-weightAssignment",1, "Weight of assignment loss")
cmd:option("-weightReconstruction", 1, "Weight of reconstruction loss")
cmd:option("-weightFeatures", 1, "Weight of feature loss")
cmd:option("-weightRegularization", 1, "Weight of Clustering Regularization")

--semisupervised learning
cmd:option("-labeled",10, "Labeled data partition (0.0-1.0 or fixed number)")
cmd:option("-proportion",10, "Number of labeled data to consider per epoch (0.0-1.0 or fixed number)")
cmd:option("-assignmentKNN", 1, "Number of neighbors to consider in assignment process")
cmd:option("-luaKNN", 1, "Use lua-knn cuda library for finding nearest neighbor")

--save results
cmd:option("-fnResults", "test_results", "Filename where final results will be saved")
cmd:option("-saveEpoch", -1, "To save results obtained at each -saveEpoch")
cmd:option("-saveResultFile", "validation_results","Filename where results will be saved at each -saveEpoch")

--others
cmd:option('-model', "semisupervised" , "Model to execute (semisupervised, supervised)")
cmd:option('-verbose', 0 , "Print extra information every epoch.")

local opt = cmd:parse(arg)

if opt.verbose == 1 then
	print(opt)
end

-- If not seed was specified, select a random one
if( opt.seed == -1 ) then
	opt.seed = torch.random()
	print("Using random seed: " .. opt.seed )
end

-- Load model
local semisupervised = false;
if( opt.model == "supervised" ) then
	require "./model/supervised"
elseif( opt.model == "semisupervised") then
	require "./model/semisupervised"
	semisupervised = true
end

----------------------------------------------------------
-- Read data
----------------------------------------------------------
local test_num_samples
local train_data, train_label
local test_data, test_label
local validation_data, validation_label
local n_samples, m_features

if( opt.dataSet == "svhn") then
    print("Loading svhn dataset...")
    local svhn_train = torch.load('./dataset/svhn/train_32x32.t7','ascii')
    local svhn_test = torch.load('./dataset/svhn/test_32x32.t7','ascii')

	-- Load train and test dataset
    train_data = svhn_train.X:double()
    train_label = svhn_train.y[1]
    test_data = svhn_test.X:double()
    test_label = svhn_test.y[1]

    test_num_samples = test_data:size(1)
    n_samples = train_data:size(1)
    m_features = train_data:size(3) * train_data:size(4)

	-- Normalize dataset
    local mean, std = normalizeTrainData(train_data)
    normalizeTestData(test_data, mean, std)

elseif( opt.dataSet == "norb") then
    print("Loading norb dataset...")
    local matio = require 'matio'
    local norb_train = matio.load('./dataset/norb/norb_train.mat')
    local norb_test = matio.load('./dataset/norb/norb_test.mat')

    -- Load train and test dataset
    train_data = norb_train.data:double()
    train_label = norb_train.label[1]:long():add(1)
    test_data = norb_test.data:double()
    test_label = norb_test.label[1]:long():add(1)

    test_num_samples = test_data:size(1) 
    n_samples = train_data:size(1)
    m_features = train_data:size(3) * train_data:size(4)

else
    print("Loading mnist dataset...")
    local mnist = require "mnist"

    -- Load train and test dataset
    train_data = mnist.traindataset().data:double():div(255)                            -- normalized [0, 1]
    train_label = mnist.traindataset().label:add(1)                                     -- label from [0,9] -> [1, 10]
    test_data = mnist.testdataset().data:double():div(255)                              -- normalized [0, 1]
    test_label = mnist.testdataset().label:add(1)                                       -- label from [0,9] -> [1, 10]

    test_num_samples = mnist.testdataset().size                                         -- test sample size
    n_samples = mnist.traindataset().size                                               -- train sample size
    m_features = mnist.traindataset().data:size(2) * mnist.traindataset().data:size(3)  -- train feature size
end

----------------------------------------------------------
-- Data Partition
----------------------------------------------------------
-- If supervised then obtain labeled data from dataset
if semisupervised == true then
	torch.manualSeed(opt.seed)
	labeled_data, labeled_label, train_data, train_label = obtainLabeledAndUnlabeledData(train_data, train_label, opt.labeled)
end

local options = {
    seed = opt.seed,
    nFolds = opt.nFolds,
    train = opt.train,
    validation = opt.validation
}

-- If validation is considered, partition the data accordingly
if( opt.validation ~= 0.0 ) then
	local foldIterator = FoldIterator(train_data, train_label, options)
	local train, validation, test = foldIterator:next()
	train_data = train.data
	train_label = train.label
	validation_data = validation.data
	validation_label = validation.label
else
-- If validation is not consider, use test as validation
	validation_data = test_data:clone()
	validation_label = test_label:clone()
end

if opt.verbose == 1 then
	print("Training Size: " .. train_data:size(1) .. ". Validation Size: " .. validation_data:size(1))
	print("Number of Samples: ".. n_samples .. ". Feature Dimension: ".. m_features)
end

----------------------------------------------------------
-- Preparing Data
----------------------------------------------------------
local inputDimension = opt.inputDimension
train_data = resizeData(train_data, inputDimension)
validation_data = resizeData(validation_data, inputDimension)
test_data = resizeData(test_data, inputDimension)

if semisupervised == true then
	labeled_data = resizeData(labeled_data, inputDimension)
	if opt.verbose == 1 then
		print("Labeled data: " .. labeled_data:size(1) )
	end
end

----------------------------------------------------------
-- Training Model
----------------------------------------------------------
local SSCVAE = SSCVAEModel(opt)

-- Use pretrained
if( opt.pretrained == 1 ) then
	SSCVAE:loadModel(opt.fnModel)
end

-- Train VAE
if semisupervised == true then
	SSCVAE:train(train_data, train_label, validation_data, validation_label, labeled_data, labeled_label)
else
	SSCVAE:train(train_data, train_label, validation_data, validation_label)
end

-- Save model
if( opt.saveModel == 1 ) then
	SSCVAE:saveModel(opt.fnModel)
end

----------------------------------------------------------
-- Testing Model
----------------------------------------------------------
print("\nTesting...")

-- Obtain probability values from categorical distribution and deterministic features
local categories, features = SSCVAE:getLatentSpace(test_data, opt.batchSizeTest )
local num_categories = opt.K

if( #opt.fnResults ~= 0 ) then
	print("Saving features and categorical values...")
	-- Save features and gumbel results (probabilities w.r.t. each cluster)
	local fileNFeaturesName = opt.fnResults .. "_Feat".. ".txt"
	local fileCategoriesName = opt.fnResults .. "_Cat".. ".txt"			
	local featuresFile = io.open(fileNFeaturesName, 'w')
	local categoriesFile = io.open(fileCategoriesName, 'w')

	for i=1,test_num_samples do 
		for j=1,num_categories do
			categoriesFile:write(categories[i][j] .. " ")
		end
		categoriesFile:write(test_label[i] .. "\n")	
	
		for j=1,features:size(2) do
			featuresFile:write(features[i][j] .. " ")
		end
		featuresFile:write(test_label[i] .. "\n")
	end
	categoriesFile:close()
	featuresFile:close()
end

-------------------------------------------------------------
-- Confusion Matrix
-------------------------------------------------------------
local test_clusters = groupClusters(categories, test_label )

io.write("            ")
for i = 1,num_categories do
   io.write(string.format("  %d    ", i - 1 ))
end
io.write("\n")

for i = 1, num_categories do
   io.write(string.format("Cluster-%d   ", i))
   if( test_clusters[i] == nil ) then
   	  print("No elements in cluster " .. i )
   else
	   local counter = {}
	   for j = 1, num_categories do
		  counter[j] = 0
	   end
	   for j= 1,#test_clusters[i] do
		  if( counter[test_clusters[i][j]] == nil ) then counter[test_clusters[i][j]] = 0 end
		   counter[ test_clusters[i][j] ] = counter[ test_clusters[i][j] ] + 1
	   end
	
	   for j = 1, num_categories do
           if #test_clusters[i] > 0 then
              io.write( string.format("%.2f   ", (counter[j]/#test_clusters[i]) ))
	       else
              io.write( string.format("%.2f   ", 0.0))
           end
       end
	   io.write("\n")
   end
end

-------------------------------------------------------------
-- Metrics
-------------------------------------------------------------
-- Calculate Clustering/Classification Accuracy
local accuracy = Classification_Score(categories, test_label:float())

-- Calculate Normalized Mutual Information (NMI)
local evaluate = require './evaluation/NMI'
local nmi = evaluate.NMIFromProbabilities(categories, test_label)

print(string.format("\nTest       Accuracy: %.5f          NMI: %.5f\n", accuracy, nmi))

