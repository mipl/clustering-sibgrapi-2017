# Taken from the original source of the work: https://arxiv.org/abs/1602.05473
# https://github.com/larsmaaloee/auxiliary-deep-generative-models/blob/master/data_loaders/norb.py
# This modification allows to save the data normalized in .mat format

import os
import numpy as np
from utils import env_paths
import gzip
from pylearn2.datasets import norb
from scipy.misc import imresize
import scipy.io as sio

#from data_helper import create_semi_supervised, pad_targets, cut_off_dataset


def _download(trainset=True, normalize=True):
    """
    Download the NORB dataset if it is not present.
    :return: The train, test and validation set.
    """

    def load_data(data_file):
        # set temp environ data path for pylearn2.
        os.environ['PYLEARN2_DATA_PATH'] = env_paths.get_data_path("norb")

        data_dir = os.path.join(os.environ['PYLEARN2_DATA_PATH'], 'norb_small', 'original')
        if not os.path.exists(data_dir):
            os.makedirs(data_dir)
        dataset = os.path.join(data_dir, data_file)

        if (not os.path.isfile(dataset)):
            import urllib
            origin = (
                os.path.join('http://www.cs.nyu.edu/~ylclab/data/norb-v1.0-small/', data_file)
            )
            print 'Downloading data from %s' % origin

            urllib.urlretrieve(origin, dataset)
        return dataset

    def unzip(path):
        with gzip.open(path, 'rb') as infile:
            with open(path.replace('.gz', ''), 'w') as outfile:
                for line in infile:
                    outfile.write(line)

    def norm(x):
        orig_shape = (96, 96)
        new_shape = (32, 32)
        x = x.reshape((-1, 2, 96 * 96))

        def reshape_digits(x, shape):
            def rebin(_a, shape):
                img = imresize(_a, shape, interp='nearest')
                return img.reshape(-1)

            nrows = x.shape[0]
            ncols = shape[0] * shape[1]
            result = np.zeros((nrows, x.shape[1], ncols))
            for i in range(nrows):
                result[i, 0, :] = rebin(x[i, 0, :].reshape(orig_shape), shape).reshape((1, ncols))
                result[i, 1, :] = rebin(x[i, 1, :].reshape(orig_shape), shape).reshape((1, ncols))
            return result

        x = reshape_digits(x, new_shape)
        x = x.reshape((-1, 2 * np.prod(new_shape)))
        x += np.random.uniform(0, 1, size=x.shape).astype('float32')  # Add uniform noise
        x /= 256.
        x -= x.mean(axis=0)

        x = x.reshape((-1, 2, 32, 32))

        x = np.asarray(x, dtype='float32')
        return x

    if trainset:
        unzip(load_data("smallnorb-5x46789x9x18x6x2x96x96-training-dat.mat.gz"))
        unzip(load_data("smallnorb-5x46789x9x18x6x2x96x96-training-cat.mat.gz"))
        dataset = norb.SmallNORB('train')
    else:
        unzip(load_data("smallnorb-5x01235x9x18x6x2x96x96-testing-dat.mat.gz"))
        unzip(load_data("smallnorb-5x01235x9x18x6x2x96x96-testing-cat.mat.gz"))
        dataset = norb.SmallNORB('test')

    x = dataset.X
    y = dataset.y

    if normalize:
        x = norm(x)

    return (x, y)

train_set = _download()
test_set = _download(trainset=False,normalize=True)
sio.savemat('norb_train.mat', {'data':train_set[0], 'label':train_set[1]})
sio.savemat('norb_test.mat', {'data':test_set[0], 'label':test_set[1]})

