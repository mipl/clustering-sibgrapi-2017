---------------------------------------------------------------------------------------
-- Util functions used in the data
---------------------------------------------------------------------------------------
-- Jhosimar Arias
---------------------------------------------------------------------------------------

-- Create a tensor with zero values, useful for zero gradient
-- 
-- Input:
--		- tensor_size: size of the tensor
--		- use_cuda: whether to create a cuda tensor or not
--
-- Output: tensor with tensor_size and filled with zero values
--
function get_zero_gradient(size, use_cuda)
	local zeroGrad
	if( use_cuda == 1 ) then
		zeroGrad = torch.CudaTensor(size):zero()
	else
		zeroGrad = torch.DoubleTensor(size):zero()
	end
	return zeroGrad
end

-- Resize data according image dimensions required for training CNNs
-- 
-- Input:
--		- data: image data [N x H x W]
--		- input_dimension: dimension to resize
--				1 for [N x (H*W)], 2 for [N x 1 x H x W ] (grayscale image), 3 considers [N x C x H x W]
--
-- Output:
--		- resized_data: obtained according to input_dimension
--		- size: depending on input_dimension
--				1 = (H * W), 2 = [H, W], 3 = [H, W]
--
function resizeData(data, inputDimension)
	if( data == nil or data:nDimension() == 0 ) then return data end
	local size
	if inputDimension == 1 then
		data = data:resize(data:size(1), data:size(2)*data:size(3))
		size = data:size(2)
	elseif inputDimension == 2 then
		size = {data:size(2), data:size(3)}
		data = data:resize(data:size(1), 1, data:size(2), data:size(3))
	else
		size = {data:size(3), data:size(4)}
	end
	return data, size
end

-- Z-score normalization of train dataset 
-- Substract the mean and divide by the standard deviation
-- 
-- Input:
--      - train_data: train dataset to normalize
--
-- Output:
--      - mean: mean obtained from the training data
--      - std: standard deviation obtained from the training data
--
function normalizeTrainData(train_data)
    local mean = {}
    local std = {}
    local channels = train_data:size(2)
    for i = 1, channels do
       -- normalize each channel globally:
       mean[i] = train_data[{ {},i,{},{} }]:mean()
       std[i] = train_data[{ {},i,{},{} }]:std()
       train_data[{ {},i,{},{} }]:add(-mean[i])
       train_data[{ {},i,{},{} }]:div(std[i])
    end
    return mean, std
end

-- Z-score normalization of test dataset 
-- Substract the mean and divide by the standard deviation
-- 
-- Input:
--      - test_data: test dataset to normalize
--      - mean: mean obtained from the training data
--      - std: standard deviation obtained from the training data
--
-- Output:
--      - normalized test dataset using mean and std from training
--
function normalizeTestData(test_data, mean, std)
    -- normalize each channel globally:
    local channels = test_data:size(2)
    for i = 1, channels do
        test_data[{ {},i,{},{} }]:add(-mean[i])
        test_data[{ {},i,{},{} }]:div(std[i])
    end
end

-- Get original data from the normalized with z-score 
--  
-- Input: 
--      - data: data to denormalize
--      - mean: mean obtained from the training data
--      - std: standard deviation obtained from the training data
--
-- Output:
--      - denormalized: denormalized dataset
--
function denormalizeData(data, mean, std)
    local channels = data:size(1)
    local denormalized = data:clone()
    for i = 1, channels do
        denormalized[{ i,{},{} }]:mul(std[i])
        denormalized[{ i,{},{} }]:add(mean[i])
    end
    return denormalized
end
