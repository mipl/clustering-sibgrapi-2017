---------------------------------------------------------------------------------------
-- Util functions to calculate distances in the data
---------------------------------------------------------------------------------------
-- Jhosimar Arias
---------------------------------------------------------------------------------------

-- Squared L2 distance
-- 
-- Input: two tensor with same size
--
-- Output: Squared l2-euclidean distance between two tensors
--
function l2(x, y)
	return torch.sum( torch.pow( (x - y), 2) )
end

-- L1 distance
-- 
-- Input: two tensor with same size
--
-- Output: l1 distance between two tensors
--
function l1(x, y)
	return torch.abs(torch.sum(x - y))
end

-- Euclidean distance
-- 
-- Input: two tensor with same size
--
-- Output: euclidean distance between two tensors
--
function euclideanDistance(x, y)
	local distance = 0
	distance = torch.sum( torch.pow( (x - y), 2) )
	return torch.sqrt(distance)
end


