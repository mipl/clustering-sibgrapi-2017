---------------------------------------------------------------------------------------
-- Inference and Generative models based on CNNs
-- Networks were based on https://github.com/Nat-D/GMVAE/blob/master/models/ConvAE.lua
---------------------------------------------------------------------------------------
-- Jhosimar Arias
---------------------------------------------------------------------------------------

local Model = {
	nFilters = 16,       -- number of filters - first convolutional layer
	nChannels = 1,       -- number of image channels
	hidden_size = 200,   -- feature size
	sigmoid_last = 0     -- wheter to use a sigmoid function in last layer
}

-- Recognizer/Inference Network
-- 
-- Input Params:
--		- image_size: size of the image [HxW]
--		- number_categories: latent space dimension (constant = K)
--
-- Input Network: Image [N x C x H x W] 
-- Output Network: 
--		- logits: logarithm of probabilities used to sample from gumbel [N x K]
--		- features: normalized features with positive values and unit norm [N x F]
--
-- H = image height, W = image width, C = number of channels
-- N = number of samples, K = number of categories, F = number of features
--
function Model:CreateRecognizer(input_size, num_categories)
	local nChannels = self.nChannels
	local nFilters = self.nFilters
	local hidden_size = self.hidden_size

	local height = input_size[1]
	local width = input_size[2]

	local input = - nn.Identity()

	-- Encoding from image to normalized features
	local features = input
				  - nn.View(-1, nChannels, height, width)
	     		  - nn.SpatialConvolution(nChannels, nFilters, 5,5,1,1,0,0)
				  - nn.SpatialBatchNormalization(nFilters)
				  - nn.ReLU(true)
				  - nn.SpatialConvolution(nFilters, 2*nFilters, 5,5,1,1,0,0)
				  - nn.SpatialBatchNormalization(2*nFilters)
				  - nn.ReLU(true)
				  - nn.SpatialConvolution(2*nFilters, 4*nFilters, 5,5,1,1,0,0)
				  - nn.SpatialBatchNormalization( 4*nFilters)
				  - nn.ReLU(true)
				  - nn.SpatialConvolution(4*nFilters, 8*nFilters, 3,3,1,1,0,0)
				  - nn.SpatialBatchNormalization( 8*nFilters)
				  - nn.ReLU(true)
				  - nn.SpatialConvolution(8*nFilters, 16*nFilters, 4,4,2,2,1,1)
				  - nn.SpatialBatchNormalization( 16*nFilters)
				  - nn.ReLU(true)
				  - nn.SpatialConvolution(16*nFilters, hidden_size, 9,9)
				  - nn.SpatialBatchNormalization( hidden_size)
				  - nn.ReLU(true)
				  - nn.View(-1, hidden_size)
				  - nn.Normalize(2)

	-- Considering deep logits for better representations
	local logit = features 
				- nn.Linear(hidden_size, hidden_size) 
				- nn.BatchNormalization( hidden_size)
				- nn.ReLU(true) 
				- nn.Linear(hidden_size, hidden_size/2 ) 
				- nn.BatchNormalization( hidden_size/2 )
				- nn.ReLU(true) 
				- nn.Linear(hidden_size/2 , num_categories)

    return nn.gModule({input}, {logit, features})
end


-- Generator network
-- params: image size(output), latent space dimension
-- input: representation z obtained after reparameterization trick
-- output: reconstruction of initial data X

-- Generator Network
-- 
-- Input Params:
--		- output_size: size of the image [HxW]
--		- number_categories: latent space dimension (constant = K)
--
-- Input Network:
--		- categories: obtained from Gumbel-Softmax [N x K]
--		- features: obtained from inference network [N x F]
--		 
-- Output Network: 
--		- image generated [N x C x H x W] 
--
-- H = image height, W = image width, C = number of channels
-- N = number of samples, K = number of categories, F = number of features
--
function Model:CreateGenerator(output_size, num_categories)
	local nChannels = self.nChannels
	local nFilters = self.nFilters
	local hidden_size = self.hidden_size

	local height = output_size[1]
	local width = output_size[2]

	local categories = - nn.Identity()
	local features = - nn.Identity()

	-- Concat categories and features
	local input = {categories, features} - nn.JoinTable(2)

	-- Generate/Reconstruct images from concated categories and features
	local generated = input
				  - nn.Linear(num_categories + hidden_size, hidden_size)
				  - nn.BatchNormalization(hidden_size)
				  - nn.ReLU(true)
				  - nn.View(-1, hidden_size, 1, 1)
				  - nn.SpatialFullConvolution(hidden_size, 16*nFilters, 9,9)
				  - nn.SpatialBatchNormalization(16*nFilters)
				  - nn.ReLU(true)
				  - nn.SpatialFullConvolution(16*nFilters, 8*nFilters, 4,4,2,2,1,1)
				  - nn.SpatialBatchNormalization( 8*nFilters)
				  - nn.ReLU(true)
				  - nn.SpatialFullConvolution(8*nFilters, 4*nFilters, 3,3,1,1,0,0)
				  - nn.SpatialBatchNormalization( 4*nFilters)
				  - nn.ReLU(true)
				  - nn.SpatialFullConvolution(4*nFilters, 2*nFilters, 5,5,1,1,0,0)
				  - nn.SpatialBatchNormalization(2*nFilters)
				  - nn.ReLU(true)
				  - nn.SpatialFullConvolution(2*nFilters, nFilters, 5,5,1,1,0,0)
				  - nn.SpatialBatchNormalization(nFilters)
				  - nn.ReLU(true)
				  - nn.SpatialFullConvolution(nFilters,nChannels, 5,5,1,1,0,0)
				  - nn.View(-1, nChannels, height, width)

	if self.sigmoid_last == 1 then
		generated = generated - nn.Sigmoid(true)
	end

	return nn.gModule({categories, features} , {generated})
end

return Model
