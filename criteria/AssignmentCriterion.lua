--------------------------------------------------------------------------------
-- Modification of Negative Log-Likelihood Loss
--------------------------------------------------------------------------------
-- Jhosimar Arias
--------------------------------------------------------------------------------

local AssignmentCriterion, parent = torch.class('nn.AssignmentCriterion', 'nn.Criterion')

-- Assignment loss obtained in the forward pass
--
-- Modification of the NLL: The normal NLL only maximizes de probability of "x_i" assigned to class "c_i"
-- NLL = -log P (c_i|x_i)
-- Now besides maximizing, we minimize the probability of "x" w.r.t. other classes
-- LA = −log P (c_i |x_i) + log P (c_j|x_i)
-- Additionaly we normalize each P(c_*|x_*) with a tanh because log tends to infinity
--
-- Input:
--		- categorical_gumbel: tensor with probabilities obtained from gumbel [N x K]
--		- assignment: table with assigned labels and distances (unlabeled only) [N x 1 x 2]
--					  [i][1][1] = assigned label
--					  [i][1][2] = nearest distance for unlabeled data
--					  [i][1][2] = -1 for labeled data
--
-- Output: loss value of the assignment loss
--
-- N = number of examples 
--
function AssignmentCriterion:updateOutput(input)
	local categorical_gumbel = input[1]
	local assignment = input[2]
	local N = categorical_gumbel:size(1)
	local num_clusters = categorical_gumbel:size(2)
	local loss = 0

	-- iterate over the assigned labels
	for index, assigned_list in pairs(assignment) do
		local assigned_label = assigned_list[1][1]
		local probability = categorical_gumbel[index][ assigned_label ]
		local p2 = probability * probability		

		-- maximize probability of the assigned cluster
		loss = loss + (1 - p2) / (1 + p2)

		-- minimize probability of other clusters
		for k=1, num_clusters do
			if( assigned_label ~= k ) then
				probability = categorical_gumbel[index][ k ]
				p2 = probability * probability
				loss = loss + (p2 - 1)/(p2 + 1) + 1
			end
		end
	end

	self.normalizing_factor = 2 * N
	self.output = loss / self.normalizing_factor
	return self.output
end


-- Gradients of the assignment loss
-- 
-- Input:
--		- anchor: input features of labeled and unlabeled data [ND x M]
--		- positive: input features of labeled data given by the assigned group for anchor [ND x M]
--		- is_labeled: tensor with 1s in the positions of labeled samples and 0s in the others [ND]
--
-- Output: matrix with gradients for each doublet [ND x M]
--
-- N = number of doublets, M = number of feature size
--
function AssignmentCriterion:updateGradInput(input)
	local categorical_gumbel = input[1]
	local assignment = input[2]
	local N = categorical_gumbel:size(1)
	local num_clusters = categorical_gumbel:size(2)

	self.gradInput = self.gradInput or categorical_gumbel.new()
	self.gradInput:typeAs(categorical_gumbel):resizeAs(categorical_gumbel):fill(0)

	-- iterate over the assigned labels
	for index, assigned_list in pairs(assignment) do

		local assigned_label = assigned_list[1][1]
		local probability = categorical_gumbel[index][ assigned_label ]

		-- gradient to maximize probability of the assigned cluster
		self.gradInput[index][ assigned_label ] = -4 * probability / self.normalizing_factor

		-- gradient to minimize probability of other clusters
		for k=1, num_clusters do
			if( assigned_label ~= k ) then
				probability = categorical_gumbel[index][ k ]
				self.gradInput[index][k] = 4 * probability / self.normalizing_factor
			end
		end
	end

	return self.gradInput	
end

