<a name="Parameters"></a>
# Parameters

Our model is based on different parameters that can be changed and executed by the `main.lua` file.

* GPU parameters:

  * `gpu`: Enables cuda, 1 for enabling and 0 for disabling. (`default: 1`);
  * `gpuID`: If one has multiple-GPUs, you can switch the default GPU. The GPU IDs are 1-indexed, so using GPU 3 can be specified as `-gpuID 3`. (`default: 1`).


* Training parameters:
  * `batchSize`: Batch size used in training. (`default: 200`);
  * `optimizer`: Specifies the method used in the optimization, you can specify it by names, so using adam can be specified as `-optimizer adam`. Different methods can be found in [optim](https://github.com/torch/optim) library. (`default: adam`);
  * `epoch`: Number of epochs to consider during training. (`default: 100`);
  * `decayEpoch`: Reduces the learning rate every `decayEpoch`. Used in *step decay* of the learning rate. For instance, `-decayEpoch 10` reduces the learning rate by `-lrDecay` every 10 epochs. (`default: 30`);
  * `learningRate`: Initial learning rate for training. (`default: 0.001`);
  * `lrDecay`: Decreases the learning rate by this value, it is used jointly with `decayEpoch`. (`default: 0.5`).


* Architecture parameters:

  * `K`: Number of clusters. (`default: 10`);
  * `inputDimension`: Dimension of the input vector into the network (`default: 2`). It converts the data size to one of the possible sizes:
        * 1 resizes to \[N x (H*W)\] (useful in FC networks).
        * 2 resizes to \[N x 1 x H x W\] (useful in CNN networks - grayscale images).
        * 3 resizes to \[N x C x H x W\] (useful in CNN networks - color images).
        - N: number of samples, H: height, W: width and C: number of channels;
  * `featureSize`: Size of the feature vector learnt by the network. (`default: 200`);
  * ~~`network`~~: Network architecture to use: currently supports only CNN. (`default: cnn`).
  * `nFilters`: Number of Convolutional Filters in first layer. (`default: 16`).


* Partition parameters:

  * `batchSizeVal`: Batch size used in validation data. (`default: 1000`);
  * `batchSizeTest`: Batch size used in test data. (`default: 1000`);
  * `train`: Percentage of data to consider for training. Range of values [0.0 - 1.0]. (`default: 0.8`);
  * `validation`: Percentage of data to consider for validation. Range of values [0.0 - 1.0]. (`default: 0.2`).

  For a partition with 80% for training and 20% for validation we specify `-train 0.8` and `-validation 0.2`. *Both train and validation must be set*.
  
* Gumbel parameters:

  * `temperature`: Initial temperature used in gumbel-softmax. Recommended range of values [0.5-1.0]. (`default: 1`);
  * `decayTemperature`: Set 1 to decay gumbel temperature every epoch. (`default: 0`);
  * `minTemperature`: Minimum temperature value after annealing. (`default: 0.5`);
  * `decayTempRate`: Rate of temperature decay every epoch. (`default: 0.00693`).

  Temperature decay is based on *Exponential decay* -> ``newTemperature = temperature * exp(-decayTempRate * currentEpoch)``. The value of `decayTempRate` can be obtained from that formulation according to the number of epochs used in training.

* Loss function paremeters:

  * `alpha`: Importance of labeled data in feature loss. (`default: 0.5`);
  * `weightAssignment`: Weight of the assignment loss. (`default: 1`);
  * `weightRegularization`: Weight of the clustering regularization loss. (`default: 1`);
  * `weightFeatures`: Weight of the feature loss. (`default: 1`);
  * `weightReconstruction`: Weight of the reconstruction loss. (`default: 1`).


* Result parameters:

  * `fnResults`: Filename to save categories and features values after testing. (`default: test_results`);
  * `saveEpoch`: Save categories and features every `saveEpoch`. Range of values [1,number of epochs]. Set -1 to not consider this parameter. (`default: -1`);
  * `saveResultFile`: Filename where categories and features will be saved every `-saveEpoch`. (`default: validation_results`).


* Semisupervised parameters:

  * `labeled`: Obtain balanced labeled data from dataset based on percentage or fixed value. Range of values: percentage [0.0-1.0]. For instance, a value of `-labeled 10` gets 10 labeled samples of each category/class. (`default:10`);
  * `proportion`: Number of labeled data to consider per epoch based on percentage or fixed value. Range of values: percentage [0.0-1.0]. For instance, a value of `-proportion 5` gets 5 labeled samples of each category per epoch. It is useful when considering large size of labeled samples and small batch size. (`default:10`);
  * `assignmentKNN`: Number of neighbors to consider in the assignment of clusters. (`default: 1`);
  * `luaKNN`: Set 1 to use lua-knn library for finding nearest neighbor. (`default: 1`).


* Pretrained parameters:

  * `pretrained`: Set 1 to use a pretrained model. (`default: 0`);
  * `saveModel`: Set 1 to save the current model. (`default: 0`);
  * `fnModel`: Filename where the model will be loaded/saved, it must have .t7 extension. (`default:pretrained/model.t7`).


* Other parameters:

  * `dataSet`: Dataset to use. It can be mnist, svhn or norb. (`default: mnist`);
  * `seed`: Seed used to initialize random weights and partitions. Set -1 for random seed. (`default: -1`);
  * `model`: Model to execute (semisupervised or supervised). (`default: semisupervised`);
  * `verbose`: Print additional details while training. (`default: 0`).
