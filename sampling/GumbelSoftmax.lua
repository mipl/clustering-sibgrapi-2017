--------------------------------------------------------------------------------
-- Sampling from Gumbel-Softmax distribution
--------------------------------------------------------------------------------
-- Jhosimar Arias
--------------------------------------------------------------------------------
-- Based on paper: Categorical Reparameterization with Gumbel-Softmax
--------------------------------------------------------------------------------

require "nn"
require "nngraph"

do
	
	local GumbelSoftmax = torch.class("GumbelSoftmax")
	local eps = 1e-12

	function GumbelSoftmax:__init() end
	
	-- Gumbel Sampling: The distribution Gumbel(0, 1) can be sampled by drawing u ∼Uniform(0, 1),
	-- and computing g = − log(− log(u)).
	--
	-- Input:
	--		- n, m: number of rows and columns
	--		- minVal, maxVal (optional): used to generate uniform numbers in a range.
	--
	-- Output:
	--		- samples from Gumbel(0, 1)
	--
	function GumbelSoftmax:SampleGumbel(n, m, minVal, maxVal) -- Fix me: only 2D
		local random_uniform
		if minVal == nil and maxVal == nil then
			random_uniform = torch.rand(n, m)
		else
			random_uniform = minVal + ((maxVal - minVal) * torch.rand(n, m))
		end
		return -torch.log( -torch.log(random_uniform + eps) + eps )
	end
	

	-- Sampling from Gumbel-Softmax distribution used to approximate a categorical distribution
	--
	-- Input:
	--		- logits: logarithm of probabilities P(X=k) (tensor)
	--		- temperature: temperature used to control the approximation (constant)
	--
	-- Output:
	--		- probabilities: samples from Gumbel-Softmax distribution (tensor)
	--
	function GumbelSoftmax:GumbelSoftmaxSample(logits, temperature)
		local G = self:SampleGumbel(logits:size(1), logits:size(2))
		local y = logits + G
		return nn.SoftMax():forward( y/temperature ) 
	end


	-- Network for sampling from Gumbel-Softmax distribution used to approximate a categorical 
	-- distribution given a constant temperature
	--
	-- N = number of examples, K = number of classes/clusters
	--
	-- Input:
	--		- logits: logarithm of probabilities P(X=k) [NxK]
	--		- gumbel_sample: samples from gumbel distribution [NxK]
	--		- temperature: temperature used to control the approximation (constant)
	--
	-- Output:
	--		- probability matrix: samples from Gumbel-Softmax distribution [NxK]
	--
	function GumbelSoftmax:GumbelSoftmaxNetwork(temperature)
		local logits = -nn.Identity()
		local gumbel_sample = -nn.Identity()
		local y = {logits, gumbel_sample} - nn.CAddTable() - nn.MulConstant(1/temperature)
		local output = y - nn.SoftMax()
		return nn.gModule({logits, gumbel_sample}, {output})
	end


	-- Network for sampling from Gumbel-Softmax distribution used to approximate a categorical 
	-- distribution with a dynamic temperature
	--
	-- N = number of examples, K = number of classes/clusters
	--
	-- Input:
	--		- logits: logarithm of probabilities P(X=k) [NxK]
	--		- gumbel_sample: samples from gumbel distribution [NxK]
	--		- temperature: temperature used to control the approximation [NxK]
	--
	-- Output:
	--		- probability matrix: samples from Gumbel-Softmax distribution [NxK]
	--
	function GumbelSoftmax:GumbelSoftmaxNetworkNoParams()
		local logits = -nn.Identity()
		local gumbel_sample = -nn.Identity()
		local temperature = -nn.Identity()
		local addition = {logits, gumbel_sample} - nn.CAddTable() 
		local y = {addition, temperature} - nn.CDivTable()
		local output = y - nn.SoftMax()
		return nn.gModule({logits, gumbel_sample, temperature}, {output})
	end	

end
