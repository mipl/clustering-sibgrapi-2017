--------------------------------------------------------------------------------
-- Fold Iterator over different partitions of the dataset
--------------------------------------------------------------------------------
-- Jhosimar Arias
--------------------------------------------------------------------------------

require "./FoldUtils"

-- FoldIterator class
-- This class allows to create k random partitions based on a seed
-- and iterate over each random partition with the method next()
-- use hashNext() to check if there are partitions left

do
    local FoldIterator = torch.class("FoldIterator")
    
    -- Initialization
	-- input: data (num_elements in first dimension), data and label must have same dimensions,
	--	options = {
	--		nFolds = number of folds, 
	--		seed = seed of random generation
	--		train = proportion of training partition (e.g 60% - 0.6)
	--		validation = proportion of validation partition (e.g 20% - 0.2)
	--		test = proportion of testing partition (e.g 20% - 0.2)
	-- }
	-- test partition may be not specified (e.g train=0.6 and validation=0.4)
	-- labels must be >= 1
    function FoldIterator:__init(data, label, options)
        
        -- Set parameter values
        self.nFolds = getParameterValue(options, "nFolds", 5)
        self.seed = getParameterValue(options, "seed", 1 )
        self.train_proportion = getParameterValue(options, "train" , 0.6 )
        self.validation_proportion = getParameterValue(options, "validation" , 0.2)
        self.test_proportion = 1.0 - (self.train_proportion + self.validation_proportion)
        self.index = 1    -- index of current fold
        self.data = data  -- num_elements x num_channels x height x width
        self.group_labels = groupByLabel(label) -- group elements
        self.num_elements = data:size(1)
        self.label = label
        
        -- random local seeds for each fold based on global seed
        torch.manualSeed(self.seed)
        self.indices_global = torch.randperm(self.num_elements):long()
    end
    
    -- Get next fold
    function FoldIterator:next()
        
        if( self.index > self.nFolds ) then error("No more Folds.") end
        
        seed_local = self.indices_global[self.index] -- local seed for partition
        self.index = self.index + 1
        
        -- Get partition indices
        train_indices, validation_indices, test_indices= partitionIndices(self.group_labels, 
            seed_local, self.train_proportion, self.validation_proportion, self.test_proportion)
        
        -- Get data according partition
        train_data = self.data:index(1, train_indices)
        train_label = self.label:index(1, train_indices)

        if(self.validation_proportion ~= 0.0 ) then
            validation_data = self.data:index(1, validation_indices)
            validation_label = self.label:index(1, validation_indices)
        else
            validation_data = torch.Tensor():type(train_data:type())
            validation_label = torch.Tensor():type(train_label:type())
        end	

        if(self.test_proportion ~= 0.0 ) then
            test_data = self.data:index(1, test_indices)
            test_label = self.label:index(1, test_indices)
        else 
            test_data = torch.Tensor():type(train_data:type())
            test_label = torch.Tensor():type(train_label:type())
        end
        
        return  formatTableData(train_data, train_label),
                formatTableData(validation_data, validation_label), 
                formatTableData(test_data, test_label)
    end
    
    -- Check if there are folds available
    function FoldIterator:hasNext()
        if( self.index <= self.nFolds ) then
            return true
        else
            return false
        end
    end

    function FoldIterator:getFoldNumber()
        return self.index
    end
    
end
