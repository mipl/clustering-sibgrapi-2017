# Learning to Cluster with Auxiliary Tasks: A Semi-Supervised Approach

This project is a torch implementation of our SIBGRAPI 2017 [paper](http://sibgrapi.sid.inpe.br/col/sid.inpe.br/sibgrapi/2017/08.21.21.30/doc/138.pdf).

### Dependencies

1. [Torch](http://torch.ch/). Install Torch by:

       ```bash
       $ git clone https://github.com/torch/distro.git ~/torch --recursive
       $ cd ~/torch;
       $ ./install.sh      # and enter "yes" at the end to modify your bashrc
       $ source ~/.bashrc
       ```

       After installing torch, you may also need install some packages using [LuaRocks](https://luarocks.org/):

       ```bash
       $ luarocks install nn
       $ luarocks install image
       ```

       To run the code on GPU you need to install cunn and cutorch:

       ```bash
       $ luarocks install cutorch
       $ luarocks install cunn
       ```

2. [lua-knn](https://github.com/Saulzar/lua-knn). It is used to compute the distance between neighbor samples. Go into the folder, and then compile it with:

       ```bash
       $ luarocks make
       ```

2. [matio-ffi](https://github.com/soumith/matio-ffi.torch). It is used to load datasets in .mat format. Run the following commands to install it:

       ```bash
       # OSX
       $ brew install homebrew/science/libmatio

       # Ubuntu
       $ sudo apt-get install libmatio2

       $ luarocks install matio
       ```

#### Docker

An image with most of the required dependencies can be obtained by pulling the following repository: [jariasf/cuda-torch](https://hub.docker.com/r/jariasf/cuda-torch/).

Alternatively, you can run the [Dockerfile](docker/Dockerfile) and build an image by yourself with all the required libraries. More details in [docker](docker/README.md) folder.

### Instructions

* To execute a single run of the semi-supervised model for a specific dataset:
  ```bash
  $ ./run.sh <dataset-name>
  ```
where dataset-name can be mnist, svhn or norb.

* To execute a single run of the supervised model for a specific dataset:
  ```bash
  $ ./run.sh <dataset-name>-supervised
  ```
  where dataset-name can be mnist or svhn.

* To change parameter values,
  ```bash
  $ ./run.sh <dataset-name> -epoch 200 -seed 50 -verbose 1
  ```
More details can be found in the [parameters](parameters.md) file.

To reproduce the results from our paper, you can run these models multiple times and then average the results. By default random seeds will be used, optionally you can specify different manual seeds by setting the ``-seed`` parameter.

Our tests were executed using cards *Titan X Pascal GPU* and *GeForce GTX 660*.

### Datasets

Tested with the following datasets: MNIST, SVHN and NORB.

* To install MNIST:

   ```bash
   $ luarocks install mnist
   ```

* The SVHN dataset can be obtained by running the file [svhn.lua](dataset/svhn.lua) located in the *dataset* folder.

* The NORB dataset can be downloaded directly from [here](https://drive.google.com/drive/folders/1MkXSlr1k8BJhC6WV3BLzyL5IKOlseS-a?usp=sharing) or can be obtained by running the file [norb.py](dataset/norb.py) located in the *dataset* folder.

### Disclaimer

The parameters specified in the ```run.sh``` file differ from those of the published paper due to additional experimental setup carried out in the master thesis: [Deep Generative Models for Clustering: A Semi-supervised and Unsupervised Approach](http://jhosimar.com/resources/master_thesis/jhosimar_thesis.pdf). An extended explanation of the paper with additional results can be found in this thesis.

* To execute a single run with the parameters of the paper for MNIST:
   ```bash
   $ ./run.sh paper-mnist
   ```

* It is possible to access to the first version of the source code with the default parameters of the paper, only allowing to run MNIST, in this [commit](https://gitlab.com/mipl/clustering-sibgrapi-2017/tree/be781409ffc870c8d69982edcb362cb5a9a69692).

### Citation

If you find our code useful in your researches, please consider citing:

    @inproceedings{AriasFigueroa2017,
        author = {Arias Figueroa, Jhosimar and Ram\'irez Rivera, A.},
        title = {Learning to Cluster with Auxiliary Tasks: A Semi-Supervised Approach},
        booktitle = {30th Conference on Graphics, Patterns and Images (SIBGRAPI 2017)},
        year = {2017}
    }
