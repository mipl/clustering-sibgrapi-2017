--------------------------------------------------------------------------------
-- Semi-supervised Categorical Variational Autoencoder - SSCVAE
--------------------------------------------------------------------------------
-- Jhosimar Arias
--------------------------------------------------------------------------------

require "nn"
require "nngraph"
require "optim"
require "image"
require "./../criteria/DiscreteKLDCriterion"
require "./../criteria/AssignmentCriterion"
require "./../criteria/EuclideanEmbedding"
require "./../sampling/GumbelSoftmax"
require "./../evaluation/ClusteringEvaluations"

do
	-- Global Variables
	local SSCVAE = torch.class("SSCVAEModel")
	local SSCVAE_model
	local Reconstruction_criteria
	local KLD_criteria
	local Assignment_criteria
	local Euclidean_criteria
	local options

	-- used to report losses in verbose mode
	local global_recon_loss, global_reg_loss, global_assign_loss, global_feat_loss
	
	-- used to evaluate NMI metric
	local evaluate = require './../evaluation/NMI'

	-- SSCVAE initialization
	-- Initialize criterions and neural networks based on desired options.
	--
	function SSCVAE:__init(_options)

		-- Initialize criterions used in loss function and sampling
		self.sampler = GumbelSoftmax()											-- Gumbel-Softmax sampling
		Reconstruction_criteria = nn.MSECriterion()								-- MSE reconstruction criterion
		Reconstruction_criteria.sizeAverage = true								-- Normalizing over the number of elements
		KLD_criteria = nn.DiscreteKLDCriterion()								-- Discrete KL divergence criterion
		Assignment_criteria = nn.AssignmentCriterion(_options.alpha)			-- Assignment criterion
		Euclidean_criteria = nn.EuclideanEmbeddingCriterion(_options.alpha)		-- Euclidean distance criterion
		
		self.num_categories = _options.K
		self.use_cuda = _options.gpu
		options = _options		

		-- Initialization for CUDA
		if self.use_cuda == 1 then
			require 'cutorch'
			require 'cunn'
			print('Using Cuda')
			cutorch.manualSeed(options.seed)
			Reconstruction_criteria:cuda()
			KLD_criteria:cuda()
			Assignment_criteria:cuda()
			cutorch.setDevice(options.gpuID)
		end

		-- Load network according dataset
		if options.dataSet == "mnist" then
			self.network_model = require "./../networks/Convolutional"
		elseif options.dataSet == "norb" then
			self.network_model = require "./../networks/Convolutional32x32"
			self.network_model.sigmoid_last = 1
		else
			self.network_model = require "./../networks/Convolutional32x32"
			self.network_model.sigmoid_last = 0
		end

		self.network_model.nFilters = options.nFilters
		self.network_model.nChannels = options.nChannels
		self.network_model.hidden_size = options.featureSize
	end


	-- Function used in optim module for optimization
	-- 
	-- Output: 
	--		- Loss obtained after forward
	--		- Gradients obtained after backward
	-- 
	function feval(x)

		if x ~= params then
			params:copy(x)
		end

		SSCVAE_model:zeroGradParameters()

		-- Perform a forward operation to the model and obtain output values
		local reconstructed, soft_logits, categorical, features = table.unpack( SSCVAE_model:forward(
																 {batch_data, batch_noise, batch_temperature}))	

		-- Data preparation
		local labeled_size = batch_subset_labels:size(1)
		local batch_size = features:size(1)
		local unlabeled_size = batch_size - labeled_size						-- Unlabeled size of current batch
		local unlabeled_data = features[{{1, unlabeled_size}}]					-- First U elements contain unlabeled data
		local labeled_data = features[{{unlabeled_size + 1, batch_size}}]		-- Last L elements contain labeled data
		local label_groups = groupByLabel(batch_subset_labels)					-- Group labels per cluster

		-- Correct indices of labeled data
		-- We do this because we grouped labeled data only with indices [1,L] but true indices are in range [U+1,U+L]
		for label, indices_list in pairs(label_groups) do
			for i=1,#indices_list do
				label_groups[label][i] = label_groups[label][i] + unlabeled_size
			end
		end

		-- 1.) Reconstruction Loss = -E[ logP(x|c,f) ]
		local reconstruction_loss = Reconstruction_criteria:forward(reconstructed , batch_data)
		local reconstruction_grad = Reconstruction_criteria:backward(reconstructed , batch_data)

		-- 2.) KL-divergence Loss = KL(Q(l|f) || P(l)) where P(l) ~ Uniform(0,1)
		local regularization_loss = KLD_criteria:forward(soft_logits)
		local regularization_grad = KLD_criteria:backward(soft_logits)

		-- 3.) Assignment Loss = Sum_i(-log P(c_i,x_i) + Sum_i!=j(log P(c_j,x_i)) )
		local assignment
		if( options.luaKNN ~= 1 ) then
			-- if nearest neighbor use a faster assignment
			if options.assignmentKNN == 1 then
				assignment = assignmentByDistanceSemiSupervised1NN(features, unlabeled_size, labeled_size, 
																	label_groups, batch_subset_labels)
			else
				assignment = assignmentKNNSemiSupervised(unlabeled_data, labeled_data, label_groups, 
														options.assignmentKNN, batch_subset_labels)
			end
		else
			assignment = fastAssignmentByDistanceSemiSupervised(unlabeled_data, labeled_data, label_groups, 
							options.assignmentKNN, batch_subset_labels)
		end
		local assignment_loss = Assignment_criteria:forward({categorical, assignment})
		local assignment_grad = Assignment_criteria:backward({categorical, assignment})

		-- 4.) Feature Loss = alpha * Sum_i( D(f_i,n_i) ) + (1-alpha) * Sum_j( D(f_j,n_j) )
		local feature_loss = Euclidean_criteria:forward({features, assignment, label_groups})
		local feature_grad = Euclidean_criteria:backward({features, assignment, label_groups})

		-- Prepare Gradients for backward operation
		local gradLoss = { options.weightReconstruction * reconstruction_grad, options.weightRegularization * regularization_grad, 
						   options.weightAssignment * assignment_grad, options.weightFeatures * feature_grad}

		-- Backpropate the gradients in the model
		SSCVAE_model:backward({batch_data, batch_noise, batch_temperature}, gradLoss)

		-- Final loss
		local loss = reconstruction_loss + regularization_loss + assignment_loss + feature_loss
		
		-- Keep global losses individually for printing in verbose mode
		global_recon_loss = global_recon_loss + reconstruction_loss 
		global_reg_loss = global_reg_loss + regularization_loss 
		global_assign_loss = global_assign_loss + assignment_loss
		global_feat_loss = global_feat_loss + feature_loss		

		return loss, gradParams
	end


	-- Train SSCVAE
	-- 
	-- Partition the data in batches and perform the training of the model according input parameters
	-- 
	-- Input: 
	--		- train_data: unlabeled data used to train the model [U x ND]
	--		- train_labels: labels of training data [U]
	--		- validation: data used to validate the model [V x ND]
	--		- validation_labels: labels of the validation data [V]
	--		- labeled_data: labeled data used to train the model [L x ND]
	--		- labeled_label: labels of the labeled data [L]
	--	
	-- U = number of unlabeled elements, L = number of labeled elements, ND = dimensions of the input data
	-- V = number of validation elements
	--
	function SSCVAE:train(train_data, train_labels, validation, validation_labels, labeled_data, labeled_label)
		
		-- Parameters used to train our model
		local batch_size = options.batchSize											
		local epochs = options.epoch
		local learning_rate = options.learningRate 
		local optimizer = options.optimizer
		local initial_temperature = options.temperature
		local use_pretrained = options.pretrained
		local batchSizeVal = options.batchSizeVal
		local decay_epoch = options.decayEpoch
		local lr_decay = options.lrDecay
		local use_pretrained = options.pretrained
		local proportion = options.proportion
		local decayTemperature = options.decayTemperature	
		local minTemperature = options.minTemperature
		local decayTempRate = options.decayTempRate		
		local verbose = options.verbose

		-- Subset of labeled data according to proportion
		local group_labels = groupByLabel(labeled_label)
		local labeled_to_train = getSubsetFromLabeledGroup(group_labels, proportion)
		local labeled_size = labeled_to_train:size(1)
		local unlabeled_size = batch_size - labeled_size

		-- Train data size and dimension
		local input_size
		if( train_data:nDimension() == 2 ) then        -- (N x num_features)
			input_size = train_data:size(2)
			self.network_model.nChannels = 1
		elseif( train_data:nDimension() == 4 ) then    -- (N x nChannels, height, width)
			input_size = {train_data:size(3), train_data:size(4)}
			self.network_model.nChannels = train_data:size(2)
		end
		self.num_samples = train_data:size(1)	

		-- Pretrained model
		if use_pretrained == nil or use_pretrained == 0 then
			SSCVAE_model = createSSCVAEModel(self, input_size, self.num_categories)
		end

		-- Gumbel temperature initialization
		local temperature = initial_temperature
		batch_temperature = torch.DoubleTensor(batch_size, self.num_categories):fill(temperature) -- temperature per batch
		
		-- Cuda variables
		if( self.use_cuda == 1 ) then
			train_labels = train_labels:cuda()
			train_data = train_data:cuda()			
			SSCVAE_model = SSCVAE_model:cuda()
			batch_temperature = batch_temperature:cuda()
			labeled_data = labeled_data:cuda()
			labeled_label = labeled_label:cuda() -- for training metrics
		end		

		-- Network parameter initialization
		params, gradParams = SSCVAE_model:getParameters()		

		-- Save loss per epoch
		self.losses = {}

		-- Save metrics per epoch, used in verbose mode
		local metrics = {}

		-- Number of iterations
		for epoch = 1,epochs do

			if epoch % decay_epoch == 0 then
				learning_rate = learning_rate * lr_decay -- annealing learning rate
			end

			-- Randomize data to get different batches at each epoch
			local indices_all = torch.randperm(self.num_samples):long()

			-- Case of num_samples not divisible by batch_size, repeat some random samples
			local indices_left = indices_all:size(1) % unlabeled_size
			if( indices_left ~= 0 ) then
				local additional = unlabeled_size - indices_left
				local additional_indices = torch.randperm(self.num_samples - indices_left ):long()
				indices_all = torch.cat( indices_all, additional_indices[{{1,additional}}])
			end

			-- Split random indices in batches
			local indices = indices_all:split(unlabeled_size)

			-- Number of batches
			local num_batches = #indices

			-- Auxiliary variables to report losses every epoch
			local Loss = 0.0
			global_recon_loss = 0.0
			global_reg_loss = 0.0
			global_assign_loss = 0.0
			global_feat_loss = 0.0
			
			-- True labels and gumbel probabilities for training data metrics
			local true_labels = torch.LongTensor(num_batches * batch_size)
			local predicted_probs = torch.FloatTensor(num_batches * batch_size, self.num_categories)

			-- Sample noise ε ~ G(0, 1) for unlabeled data
			local gumbel_noise = self.sampler:SampleGumbel(self.num_samples, self.num_categories)   
			if( self.use_cuda == 1 ) then
				gumbel_noise = torch.CudaTensor():resize(self.num_samples, self.num_categories):copy(gumbel_noise)
			end

			-- Sample noise ε ~ G(0, 1) for labeled data
			local gumbel_noise_labels = self.sampler:SampleGumbel(labeled_size * num_batches, self.num_categories)
			if( self.use_cuda == 1 ) then
				gumbel_noise_labels = torch.CudaTensor():resize(labeled_size * num_batches, self.num_categories):copy(gumbel_noise_labels)
			end

			-- We use always the same labeled data, otherwise this assigment should go inside the training per batch
			local labeled_indices = getSubsetFromLabeledGroup(group_labels,proportion)
			local labeled_subset_data = labeled_data:index(1,labeled_indices)
			batch_subset_labels = labeled_label:index(1,labeled_indices)

			-- Create batch data tensor according size of input data
			if(train_data:nDimension() == 2 ) then
				batch_data = torch.Tensor(batch_size, input_size):typeAs(train_data)
			elseif( train_data:nDimension() == 4 ) then
				batch_data = torch.Tensor(batch_size, self.network_model.nChannels, input_size[1], input_size[2] ):typeAs(train_data)
			end

			-- Create batch noise and labels according to batch_size
			batch_noise = torch.Tensor(batch_size, self.num_categories):typeAs(gumbel_noise)
			batch_labels = torch.Tensor(batch_size):typeAs(train_labels)

			-- Fix labeled data and assign it to last L elements of the batch
			batch_data[{{unlabeled_size + 1, batch_size}}] = labeled_subset_data
			batch_labels[{{unlabeled_size + 1, batch_size}}] = batch_subset_labels

			-- Training in batches
 			for t,v in ipairs(indices) do
				-- Use training mode required to use Batch Normalization layer
				SSCVAE_model:training()
				xlua.progress(t, #indices)

				if batch_size < labeled_label:size(1) then
					-- We use always the same labeled data, otherwise this assigment should go inside the training per batch
					local labeled_indices = getSubsetFromLabeledGroup(group_labels,proportion)
					local labeled_subset_data = labeled_data:index(1,labeled_indices)
					batch_subset_labels = labeled_label:index(1,labeled_indices)
					batch_data[{{unlabeled_size + 1, batch_size}}] = labeled_subset_data
					batch_labels[{{unlabeled_size + 1, batch_size}}] = batch_subset_labels
				end

				-- Assign unlabeled data to the first U elements of the batch
				batch_data[{{1, unlabeled_size}}] = train_data:index(1, v)
				batch_noise[{{1, unlabeled_size}}] = gumbel_noise:index(1,v)
				batch_noise[{{unlabeled_size + 1, batch_size}}] = gumbel_noise_labels[{{labeled_size*(t-1) + 1, labeled_size*t}}]
				batch_labels[{{1, unlabeled_size}}] = train_labels:index(1, v) -- for training metrics

				-- Optimize the model with the loss function defined in feval function
				__, loss = optim[optimizer](feval, params, {learningRate = learning_rate})
				Loss = Loss + loss[1]
			end

			self.losses[#self.losses + 1] = Loss		
			
			-- Training loss information			
			print(string.format("Epoch: %d  Loss: %.6f", epoch, Loss/num_batches))
			
			if( verbose == 1 ) then
				print(string.format("Reconstruction: %.5f   Categorical: %.5f   Assignment: %.5f   Feature: %.5f",
									global_recon_loss/num_batches, global_reg_loss/num_batches, 
									global_assign_loss/num_batches, global_feat_loss/num_batches)) 
			end

			-- Predict probabilities per cluster for training data
			local predicted_probs, _ = predictData(SSCVAE_model, train_data, batchSizeVal, self.num_categories, self.use_cuda)
            
			-- Training metrics
			local accuracy_training = Classification_Score(predicted_probs, train_labels:float())
			local nmi_training = evaluate.NMIFromProbabilities(predicted_probs, train_labels)
			print(string.format("Training   Accuracy: %.5f          NMI: %.5f", accuracy_training, nmi_training))

			-- Save metrics every 10 epochs to print in verbose mode
			if( epoch == 1 or epoch % 10 == 0 ) then
				metrics[ epoch ] = {}
				metrics[ epoch ][1] = accuracy_training 
			end

			-- Validation data information and performance
			if( validation ~= nil and validation:size(1) > 0 ) then
				
				-- Predict categories and features for validation data
				local categories, features = predictData(SSCVAE_model, validation, batchSizeVal, self.num_categories, self.use_cuda)

				-- Save data each saveEpoch iteration
				if( options.saveEpoch > 0 and options.saveEpoch <= epochs and epoch % options.saveEpoch == 0 ) then
					----------------------------------------------------------------------------------------
					local validation_num_samples = validation:size(1)
					local validation_true_label = validation_labels[{{1,validation_num_samples}}]

					local fileNFeaturesName = options.saveResultFile .. "_Feat_".. epoch.. ".txt"
					local fileCategoriesName = options.saveResultFile .. "_Cat_".. epoch.. ".txt"			
					
					print("Saving results of epoch: " .. epoch .. " in file: ".. options.saveResultFile .. "...")
					local featuresFile = io.open(fileNFeaturesName, 'w')
					local categoriesFile = io.open(fileCategoriesName, 'w')

					for i=1,validation_num_samples do 
						for j=1,features:size(2) do
							featuresFile:write(features[i][j] .. " ")
						end
						featuresFile:write(validation_true_label[i] .. "\n")

						for j=1,self.num_categories do
							categoriesFile:write(categories[i][j] .. " ")
						end
						categoriesFile:write(validation_true_label[i] .. "\n")

					end
					categoriesFile:close()
					featuresFile:close()
					-----------------------------------------------------------------------------------------
				end
				
				-- Validation metrics
				local accuracy = Classification_Score(categories, validation_labels:float())
				local nmi = evaluate.NMIFromProbabilities(categories, validation_labels)
				if options.validation == 0.0 then
					print(string.format("Test       Accuracy: %.5f          NMI: %.5f", accuracy, nmi))
				else
					print(string.format("Validation Accuracy: %.5f          NMI: %.5f", accuracy, nmi))
				end
				
				-- Save metrics every 10 epochs to print in verbose mode
				if( epoch == 1 or epoch % 10 == 0 ) then
					metrics[ epoch ][2 ] = accuracy
					metrics[ epoch ][3 ] = nmi  
				end

			end
			
			-- annealing gumbel temperature rate -> alpha = alpha0 * exp(-kt). alpha0=initial, t=iteration/epoch
			if( decayTemperature == 1 ) then
				--exponential annealing for temperature
				temperature = math.max( initial_temperature * math.exp( -decayTempRate * epoch ), minTemperature )
				batch_temperature:fill(temperature)
				options.temperature = temperature
				print("Gumbel temperature: ".. temperature)
			end
			
			-- Print log of metrics (verbose mode)	
			if( verbose == 1 ) then
				for iteration, values in pairs(metrics) do
					io.write(string.format("(%d): TACC: %.5f  VACC: %.5f VNMI: %.5f, ", 
							iteration, values[1], values[2], values[3]))
				end
				io.write("\n")
			end

		end
	end


	-- SSCVAE Model
	-- 
	-- Partition the data in batches and perform the training of the model according input parameters
	-- 
	-- Params:
	--		- input_size: size of input data used in creation of networks
	--		- num_categories: number of categories used in creation of networks
	--
	-- Input Network: 
	--		- input_data: data to pass throught the model [N x ND]
	--		- gumbel_noise: random noise obtained from G(0,1) [N x K]
	--		- temperature: temperature used in gumbel-softmax [N x K]
	--	
	-- Output Network:
	--		- reconstructed: reconstructed image after forward [N x ND]
	--		- soft_logits: softmax of logits i.e. probabilities [N x K]
	--		- categorical: probabilities after gumbel-softmax [N x K]
	--		- features: normalized features with positive values and unit norm [N x F]
	--
	-- N = number of samples per batch, ND = dimensions of the input data
	-- K = number of clusters, F = number of features
	--
	function createSSCVAEModel(self, input_size, num_categories)
		local input_data = - nn.Identity()
		local gumbel_noise = - nn.Identity()	
		local temperature = - nn.Identity()
		
		-- Inference network receives input_data
		local recognizer = self.network_model:CreateRecognizer(input_size, num_categories)
		local output_recognizer = input_data - recognizer

		-- Obtain logits and features
		local logits = output_recognizer - nn.SelectTable(1)
		local features = output_recognizer - nn.SelectTable(2)
		
		-- Get probabilities from logits by using SoftMax
		local soft_logits = logits - nn.SoftMax()

		-- Get categorical data using gumbel-softmax distribution
		local categorical = {logits, gumbel_noise, temperature} - self.sampler:GumbelSoftmaxNetworkNoParams()

		-- Generator network receives categorical probabilities and normalized features		
		self.generator = self.network_model:CreateGenerator(input_size, num_categories)
		local reconstructed = {categorical, features} - self.generator

		return nn.gModule({input_data, gumbel_noise, temperature}, {reconstructed, soft_logits, categorical, features})
	end


	-- Data Reconstruction
	-- 
	-- Obtain reconstructed image for different inputs after training model
	-- 
	-- Input: 
	--		- input_data: data to pass throught the model [1 x ND] 
	--					(1 x num_features or 1 x 1 x height x width)
	--	
	-- Output:
	--		- reconstructed: reconstructed image after forward [1 x ND]
	--
	-- N = number of samples per batch, ND = dimensions of the input data
	--
	function SSCVAE:reconstruct(input_data)	

		-- generate gumbel noise ε ~ G(0, 1)
		local gumbel_noise = self.sampler:SampleGumbel(1, self.num_categories)
		
		-- Temperature per batch - default 0.5
		local temperature = torch.DoubleTensor(1, self.num_categories):fill(options.temperature)

		-- Cuda variables
		if self.use_cuda == 1 then	
			input_data = input_data:cuda() 
			gumbel_noise = torch.CudaTensor():resize(1, self.num_categories):copy(gumbel_noise)
			temperature = temperature:cuda()
		end 

		-- Used to evaluate model required to use Batch Normalization layer
		SSCVAE_model:evaluate()
		
		-- Obtain reconstructed image
		local reconstructed, _, _ = table.unpack(SSCVAE_model:forward({input_data, gumbel_noise, temperature}))
		if( inputDimension == 2 ) then -- 1 x 1 x height x width 
			return reconstructed[1]:double()
		end
		
		return reconstructed:double()
	end
	

	-- Predict features and categories for a given input data
	-- 
	-- Obtain both categories and features after forward pass
	-- 
	-- Input: 
	--		- input_data: data to pass throught the model [N x ND] 
	--					(N x num_features or N x 1 x height x width)
	--		- batch_size: used to predict values in batches
	--	
	-- Output:
	--		- categorical: probabilities after gumbel-softmax [N x K]
	--		- features: normalized features with positive values and unit norm [N x F]
	--
	-- N = number of samples per batch, ND = dimensions of the input data
	-- K = number of clusters, F = numbe of features
	-- 
	function SSCVAE:getLatentSpace(input_data, batch_size)
		return predictData(SSCVAE_model, input_data, batch_size, self.num_categories, self.use_cuda)
	end

	-- Save trained model, useful for pretraining
	-- 
	-- Input: path to save the model
	--
	function SSCVAE:saveModel(path) 		
		SSCVAE_model:clearState() 		
		torch.save(path, SSCVAE_model) 	
	end 	

	-- Load trained model
	-- 
	-- Input: path to load the trained model
	--
	function SSCVAE:loadModel(path) 		
		SSCVAE_model = torch.load(path) 	
	end
	

	-- Predict features and categories for a given input data
	-- 
	-- Obtain both categories and features after forward pass
	-- 
	-- Input: 
	--		- model after training
	--		- input_data: data to pass throught the model [N x ND] 
	--					(N x num_features or N x 1 x height x width)
	--		- batch_size: used to predict values in batches
	--		- num_categories: number of categories/clusters
	--		- use_cuda: whether to use cuda or not (it should follow model training)
	--	
	-- Output:
	--		- categorical: probabilities after gumbel-softmax [N x K]
	--		- features: normalized features with positive values and unit norm [N x F]
	--
	-- N = number of samples per batch, ND = dimensions of the input data
	-- K = number of clusters, F = numbe of features
	-- 
	function predictData(SSCVAE_model, input_data, batch_size, num_categories, use_cuda)
		-- default batch size prediction
		if( batch_size == nil ) then batch_size = 100 end

		local num_samples = input_data:size(1)
		local sampler = GumbelSoftmax()
		
		-- generate gumbel noise ε ~ G(0, 1)
		local gumbel_noise = sampler:SampleGumbel(num_samples, num_categories)

		-- Temperature per batch
		local temperature = torch.DoubleTensor(num_samples, num_categories):fill(options.temperature)
		
		-- Cuda variables
		if use_cuda == 1 then	
			input_data = input_data:cuda() 
			gumbel_noise = torch.CudaTensor():resize(num_samples, num_categories):copy(gumbel_noise)
			temperature = temperature:cuda()
		end
		
		-- Split in batches following the input order
		local indices_all = torch.linspace(1, num_samples, num_samples):long()
		local indices = indices_all:split(batch_size)

		-- Tensor to save the results
		local categorical = torch.DoubleTensor()
		local features = torch.DoubleTensor()

		-- Used to evaluate model required to use Batch Normalization layer
		SSCVAE_model:evaluate()
		
		-- Testing in batches
		for t,v in ipairs(indices) do 
			local batch_data = input_data:index(1, v)
			local batch_noise = gumbel_noise:index(1,v)
			local batch_temperature = temperature:index(1,v)
			local _,_, sample, feat = table.unpack(SSCVAE_model:forward({batch_data, batch_noise, batch_temperature}))
			categorical = torch.cat(categorical, sample:double(),1)
			features = torch.cat(features, feat:double(), 1)
		end
		return categorical, features
	end	


	-- Predict features for a given input data
	-- 
	-- Obtain features after forward pass
	-- 
	-- Input: 
	--		- input_data: data to pass throught the model [N x ND] 
	--					(N x num_features or N x 1 x height x width)
	--	
	-- Output:
	--		- features: normalized features with positive values and unit norm [N x F]
	--
	-- N = number of samples per batch, ND = dimensions of the input data
	-- K = number of clusters, F = numbe of features
	-- 
	function SSCVAE:getFeatures(input_data)
		-- Generate gumbel noise ε ~ G(0, 1)
		local gumbel_noise = self.sampler:SampleGumbel(input_data:size(1), self.num_categories)

		-- Temperature per batch - default 0.5
		local temperature = torch.DoubleTensor(input_data:size(1), self.num_categories):fill(options.temperature)

		-- Cuda variables
		if self.use_cuda == 1 then	
			input_data = input_data:cuda() 
			gumbel_noise = torch.CudaTensor():resize(input_data:size(1), self.num_categories):copy(gumbel_noise)
			temperature = temperature:cuda()
		end 

		-- Obtain features from input data
		local _,_,_,features = table.unpack(SSCVAE_model:forward({input_data, gumbel_noise, temperature}))
		return features
	end

end
