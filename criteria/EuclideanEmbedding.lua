--------------------------------------------------------------------------------
-- Euclidean Embedding Criterion 
--------------------------------------------------------------------------------
-- Jhosimar Arias
--------------------------------------------------------------------------------

local EuclideanEmbeddingCriterion, parent = torch.class('nn.EuclideanEmbeddingCriterion', 'nn.Criterion')

-- Constructor which initializes tensors and assigns the importance parameter if required
function EuclideanEmbeddingCriterion:__init(alpha)
	parent.__init(self)
	self.alpha = alpha											-- importance parameter
end


-- Feature distance loss obtained in the forward pass
-- 
-- Input:
--		- anchor: input features of labeled and unlabeled data [ND x M]
--		- positive: input features of labeled data given by the assigned group for anchor [ND x M]
--		- is_labeled (optional): tensor with 1 in the positions of labeled samples and 0 in the others
--
-- Output: loss value of the divergence between input and uniform prob.
--
-- N = number of doublets, M = number of feature size
--
function EuclideanEmbeddingCriterion:updateOutput(input)
	local features = input[1]
	local assignment = input[2]
	local label_groups = input[3]

	-- Create doublets containing anchor and positive values that will be embedded closely
	self.doublet, self.doublet_indices = calculateDoublet(features, assignment, label_groups)

	local a = self.doublet[1]									-- anchor
	local p = self.doublet[2]									-- positive
	local N = a:size(1)
	local upper_bound = torch.sqrt(2)							-- upperbound considering values of vectors [0,1]

	self.diff = torch.csub(a, p)								-- (anchor - positive)
	self.dist = self.diff:norm(2,2) 							-- euclidean distance = || anchor - positive ||

	if( 0 <= self.alpha and self.alpha <= 1 ) then 				-- if importance parameter is used
		local isLabeled = self.doublet[3]

		local labeled_dist = self.dist[ isLabeled:eq(1) ]		-- distances of labeled data
		local unlabeled_dist = self.dist[ isLabeled:eq(0) ]		-- distances of unlabeled data
		local num_labeled = labeled_dist:size(1)
		local num_unlabeled = 1
		if( unlabeled_dist:dim() > 0 ) then 
			num_unlabeled = unlabeled_dist:size(1)
		end

		self.normalizing_factor_labeled   = ( upper_bound * num_labeled )
		self.normalizing_factor_unlabeled = ( upper_bound * num_unlabeled )
		
		-- Loss = α * labeled_distances + (1 - α) * unlabeled_distances
		self.output = self.alpha * labeled_dist:sum() / self.normalizing_factor_labeled +
     				(1 - self.alpha) * unlabeled_dist:sum() / self.normalizing_factor_unlabeled

	else														-- case without importance parameter
		self.normalizing_factor = upper_bound * N
		self.output = self.dist:sum() / self.normalizing_factor
	end

	return self.output
end


-- Gradients of the euclidean distance for each doublet
-- 
-- Input:
--		- anchor: input features of labeled and unlabeled data [ND x M]
--		- positive: input features of labeled data given by the assigned group for anchor [ND x M]
--		- is_labeled: tensor with 1s in the positions of labeled samples and 0s in the others [ND]
--
-- Output: matrix with gradients for each doublet [ND x M]
--
-- N = number of doublets, M = number of feature size
--
function EuclideanEmbeddingCriterion:updateGradInput(input)
	local features = input[1]
	local label_groups = input[3]
	local N = features:size(1)

	self.dist = self.dist + self.dist:eq(0):typeAs(self.dist) 	-- gradient of euclidean distances considering case of distance = 0

	local doublet_grad = {}
	doublet_grad[1] = torch.cdiv(self.diff, self.dist:expand(self.diff:size()))	-- gradient w.r.t. anchor
	doublet_grad[2] = -doublet_grad[1]											-- gradient w.r.t. positive

	-- Final gradient containing information of all the doublets
	self.gradInput = torch.Tensor(features:size()):typeAs(features):fill(0)
	self.gradInput = updateIndicesFromDoublets(self.gradInput, doublet_grad, self.doublet_indices, false)

	if( 0 <= self.alpha and self.alpha <= 1 ) then 				-- if importance parameter is used
		local L = 0												-- labeled size
		for label, indices_list in pairs(label_groups) do
			L = L + #indices_list
		end

		local U = N - L											-- unlabeled size
		self.gradInput[{{U + 1, N}}] = self.gradInput[{{U + 1, N}}] * self.alpha/ self.normalizing_factor_labeled  -- labeled case
		if( U > 1 ) then
			self.gradInput[{{1, U}}] = self.gradInput[{{1, U}}] * (1 - self.alpha)/ self.normalizing_factor_unlabeled  -- unlabeled case
		end
	else														-- case without importance parameter
		self.gradInput = self.gradInput/ self.normalizing_factor
	end

	return self.gradInput
end

