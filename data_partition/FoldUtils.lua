--------------------------------------------------------------------------------
-- Util functions used in FoldIterator file
--------------------------------------------------------------------------------
-- Jhosimar Arias
--------------------------------------------------------------------------------


-- Group all indices of data by label
-- input: label data
-- output: table with key=label and value=list of indices
-- Example: input = [1,3,5,1,2,3,5]
-- output = {1:{1,4}, 2:{5}, 3:{2,6}, 5:{3,7}}
function groupByLabel(data_label)
    local labels = {}
    local num_elements = data_label:size(1)
    for i = 1,num_elements do
        local key = data_label[i]
        if labels[key] == nil then
            labels[key] = {}
        end
        local key_list = labels[key]
        labels[key][#key_list + 1] = i
    end
    return labels
end


-- Partition group of indices in train, validation and test
-- input: group of label indices, local seed for each fold, proportions [0-1]
-- ouput: tables of indices (train/test/validation)
function partitionIndices(group_labels, seed_local, train_proportion, validation_proportion, test_proportion)
    local train_indices = torch.LongTensor()
    local test_indices = torch.LongTensor()
    local validation_indices = torch.LongTensor()
    
    -- for each label list, partition data given proportions
    for key, key_list in pairs(group_labels) do 
        local num_elements_class = #key_list
        
		-- random local seed of current fold
        torch.manualSeed(seed_local)
        local indices_random = torch.randperm(num_elements_class):long()
   	
		-- partition sizes
        local train_partition_size = math.floor(num_elements_class * train_proportion)
        local validation_partition_size = math.floor(num_elements_class * validation_proportion + 0.5 )
        local test_partition_size = num_elements_class - train_partition_size - validation_partition_size

		-- select from key_list according to random indices generated
		key_list = torch.Tensor(key_list):long()
        train_indices = torch.cat( train_indices, key_list:index(1,indices_random[{{1,train_partition_size}}]) )
        
        local initial = train_partition_size
        local final = initial + validation_partition_size

		if(test_proportion == 0.0 or test_partition_size <= 0) then final = num_elements_class end --only training and validation

        if(validation_proportion ~= 0.0 and validation_partition_size > 0) then
            validation_indices = torch.cat( validation_indices,  key_list:index(1, indices_random[{{ initial + 1 , final }}]) )
        end    
        
        if(test_proportion ~= 0.0 and test_partition_size > 0) then
            test_indices = torch.cat( test_indices,  key_list:index(1,indices_random[{{final + 1, num_elements_class}}]) )
        end
    end
    
    return train_indices, validation_indices, test_indices
end

-- Get parameters of dictionary
-- input: dictionary, key to get values and default value in case key doesn't exist
-- output: value of key in dictionary
function getParameterValue(options, key, default)
    value = default
    if( options[key] ~= nil ) then value = options[key] end
    return value
end

-- Create a table with data, size and label keys
function formatTableData(data, label)
    size = 0
    if( data:nDimension() > 0 ) then size = data:size(1) end
    local table = { data = data, size = size ,  label = label }
    return table
end


function partitionData(data, label, group_labels, seed_local, 
        train_proportion, validation_proportion, test_proportion)

    train_indices, test_indices, validation_indices = partitionIndices(group_labels, 
    seed_local, train_proportion, validation_proportion, test_proportion)
        
    train_data = data:index(1, train_indices)
    train_label = label:index(1, train_indices)

    validation_data = data:index(1, validation_indices)
    validation_label = label:index(1, validation_indices)

	if(validation_proportion ~= 0.0 ) then
	    validation_data = data:index(1, validation_indices)
	    validation_label = label:index(1, validation_indices)
	else
		validation_data = torch.Tensor():type(train_data:type())
        validation_label = torch.Tensor():type(train_label:type())
	end	

    if(test_proportion ~= 0.0 ) then
        test_data = data:index(1, test_indices)
        test_label = label:index(1, test_indices)
    else
        test_data = torch.Tensor():type(train_data:type())
        test_label = torch.Tensor():type(train_label:type())
    end
    
    return formatTableData(train_data, train_label),
           formatTableData(validation_data, validation_label), 
           formatTableData(test_data, test_label)
end

