---------------------------------------------------------------------------------------
-- Util functions used in semi-supervised data
---------------------------------------------------------------------------------------
-- Jhosimar Arias
---------------------------------------------------------------------------------------

-- Partition data into labeled and unlabeled data given percentage or labeled size per class
-- 
-- Input:
--		- data: input data to create partitions [N x D* ]
--		- labels: inputa data labels [N]
--		- proportion of the labeled [0,1] - 0.3 means 30% of labeled data or 
--		  number of labeled data per class - 10 means 10 labels per class
--
-- Output: 
--		- labeled_data: data partition with labeled data according to proportion
--		- labeled_data_label: labels of labeled_data
--		- unlabeled_data: data partition with unlabeled data with the rest of the data
--		- unlabeled_data_label: labels of the unlabeled_data for metrics
--
-- N = number of doublets, D* = other dimensions of the input data
--
function obtainLabeledAndUnlabeledData(data, labels, proportion)

	-- group data by labels
	local group_labels = groupByLabel(labels)
	local labeled_indices = torch.LongTensor()
	local unlabeled_indices = torch.LongTensor()

    -- for each label list, partition data given proportions
	for _, key_list in pairs(group_labels) do
		local num_elements_class = #key_list
		local indices_random = torch.randperm(num_elements_class):long()

		-- consider all elements of a cluster if their number of elements is less than proportion
		local labeled_partition_size = math.min( num_elements_class, proportion)

		-- consider proportion as percentage
		if( proportion <= 1 ) then
			labeled_partition_size = math.floor(num_elements_class * proportion)
		end
		local unlabeled_partition_size = num_elements_class - labeled_partition_size

		-- select from key_list according to random indices generated
		key_list = torch.Tensor(key_list):long()

		if( proportion ~= 0.0 and labeled_partition_size > 0 ) then
			labeled_indices = torch.cat( labeled_indices, key_list:index(1,indices_random[{{1,labeled_partition_size}}]) )
			-- if we have more elements to get unlabeled data after obtaining the labeled ones
			if( num_elements_class > labeled_partition_size + 1 ) then
				unlabeled_indices = torch.cat( unlabeled_indices, key_list:index(1,indices_random[{{labeled_partition_size + 1,num_elements_class}}]) )
			end
		else
			-- consider only unlabeled data
			unlabeled_indices = torch.cat( unlabeled_indices, key_list:index(1,indices_random[{{1,num_elements_class}}]) )
		end
	end
    
	-- save partitioned data into tensors separately
	local labeled_data = torch.Tensor():type(data:type())
	local labeled_data_label = torch.Tensor():type(labels:type())	
	local unlabeled_data = torch.Tensor():type(data:type())
	local unlabeled_data_label = torch.Tensor():type(labels:type()) 		

	if( labeled_indices:dim() > 0 and labeled_indices:size(1) > 0 ) then 
		labeled_data = data:index(1, labeled_indices) 
		labeled_data_label = labels:index(1, labeled_indices)
	end
	
	if( unlabeled_indices:dim() > 0 and unlabeled_indices:size(1) > 0 ) then 
		unlabeled_data = data:index(1, unlabeled_indices) 
		unlabeled_data_label = labels:index(1, unlabeled_indices)
	end	
	
	return labeled_data, labeled_data_label, unlabeled_data, unlabeled_data_label
end


-- Assignment of data based on KNN algorithm
--
-- Assign labels to unlabeled data based on KNN algorithm w.r.t. labeled data
-- distances are kept for unlabeled data, for the labeled a -1 is used to differentiate it
-- to the unlabeled data
--
-- Input:
--		- unlabeled_data: tensor with unlabeled features [U x F]
--		- labeled_data: tensor with labeled features [L x F]
--		- label_groups: table with pair <label, indices list w.r.t. data_labels>
--		- k: parameter to select the kth nearest neighbor
--		- data_labels: true labels of all the data [N] -> N = U + L
--
-- Output: 
--		- assignment: table with assigned labels and distances (unlabeled only) [N x 1 x 2]
--					  [i][1][1] = assigned label
--					  [i][1][2] = nearest distance for unlabeled data
--					  [i][1][2] = -1 for labeled data
--
-- U = number of unlabeled samples, L = number of labeled samples, F = number of features
-- N = total number of elements (U+L)
--
function assignmentKNNSemiSupervised(unlabeled_data, labeled_data, label_groups, k, data_labels)

	local unlabeled_size = unlabeled_data:size(1)
	local assignment = {}

	-- iterate over all unlabeled data to assign a label
	for index=1,unlabeled_size do
		assignment[index] = {}
		local knn_distances = {}	
	
		-- iterate over each cluster and keep all the distances w.r.t. all other elements		
		for label,indices_label_list in pairs(label_groups) do
			-- iterate over each element of the cluster "label"
			for j=1, #indices_label_list do
				local label_index = indices_label_list[j] - unlabeled_size
				local distance = euclideanDistance(unlabeled_data[index], labeled_data[label_index] )
				local size = #knn_distances + 1
				knn_distances[ size ] = {}
				knn_distances[ size ][1] = distance
				knn_distances[ size ][2] = label
			end
		end

		-- sort by distance in increasing order
		table.sort( knn_distances, function( a,b ) return a[1] < b[1] end )

		local average_distance = 0	

		-- if knn elements is less than parameter k, use all the knn elements
		local knn = math.min(k, #knn_distances)
		local countLabel = {}
		local maximum, maximum_label, distance = 0, -1, -1

		-- assign label based on the most common element
		for j=1,knn do
			if countLabel[ knn_distances[j][2] ] == nil then
				countLabel[ knn_distances[j][2] ] = 0
			end
			countLabel[ knn_distances[j][2] ] = countLabel[ knn_distances[j][2] ] + 1
			if( countLabel[ knn_distances[j][2] ] > maximum ) then
				maximum = countLabel[ knn_distances[j][2] ]
				maximum_label = knn_distances[j][2]
				distance = knn_distances[j][1]
			end
		end

		-- keep the label in position 1 and distance in position 2
		assignment[index] = {}
		assignment[index][1] = {}
		assignment[index][1][1] = maximum_label
		assignment[index][1][2] = distance
	end
	
	-- for labeled data, assign directly its true label with distance -1
	for i=1,labeled_data:size(1) do
		local correct_index = i + unlabeled_size
		assignment[correct_index] = {}
		assignment[correct_index][1] = {}
		assignment[correct_index][1][1] = data_labels[i]
		assignment[correct_index][1][2] = -1
	end
	return assignment
end


-- Assignment of data based on 1-NN algorithm
--
-- * Assign labels to unlabeled data based on 1-NN algorithm w.r.t. labeled data
-- distances are kept for unlabeled data, for the labeled a -1 is used to differentiate it
-- to the unlabeled data.
-- * We assume that the first U elements of the data are unlabeled and the next L elements
-- are labeled.
--
-- Input:
--		- features: tensor with the all the features of the data [N x F]
--		- unlabeled_size: number of elements of the unlabeled data (constant = U)
--		- labeled_size: number of elements of the labeled data (constant = L)
--		- label_groups: table with pair <label, indices list w.r.t. data_labels>
--		- data_labels: true labels of all the data [N] -> N = U + L
--
-- Output: 
--		- assignment: table with assigned labels and distances (unlabeled only) [N x 1 x 2]
--					  [i][1][1] = assigned label
--					  [i][1][2] = nearest distance for unlabeled data
--					  [i][1][2] = -1 for labeled data
--
-- U = number of unlabeled samples, L = number of labeled samples, F = number of features
-- N = total number of elements (U+L)
--
function assignmentByDistanceSemiSupervised1NN(features, unlabeled_size, labeled_size, label_groups, data_labels)
	local assignment = {}

	-- iterate over all unlabeled data to assign a label
	for index=1,unlabeled_size do
		assignment[index] = {}

		-- iterate over each cluster and get the nearest neighbor	
		for label,indices_label_list in pairs(label_groups) do
			local minimum_distance = 0
			for j=1, #indices_label_list do
				local label_index = indices_label_list[j]

				-- compare elements using squared euclidean distance
				local distance = l2(features[index], features[label_index])				            
				if( j == 1 or distance < minimum_distance ) then
					minimum_distance = distance
				end
			end

			-- compare and update the minimum distance w.r.t. the current cluster
			if( #assignment[index] == 0 or minimum_distance < assignment[index][1][2] ) then
				assignment[index] = {}
				assignment[index][1] = {}
				assignment[index][1][1] = label
				assignment[index][1][2] = minimum_distance
			elseif( minimum_distance == assignment[index][1][2] ) then
				local size = #assignment[index]
				assignment[index][ size + 1 ] = {}
				assignment[index][ size + 1 ][1] = label
				assignment[index][ size + 1 ][2] = minimum_distance
			end

		end
	end
	
	-- for labeled data, assign directly its true label with distance -1
	for i=1,labeled_size do
		local correct_index = i + unlabeled_size
		assignment[correct_index] = {}
		assignment[correct_index][1] = {}
		assignment[correct_index][1][1] = data_labels[i]
		assignment[correct_index][1][2] = -1
	end
	return assignment
end


-- Fast Assignment of data based on k-NN algorithm (requires CUDA)
-- It requires the library "lua-knn: https://github.com/Saulzar/lua-knn" 
--
-- * Assign labels to unlabeled data based on KNN algorithm w.r.t. labeled data
-- distances are kept for unlabeled data, for the labeled a -1 is used to differentiate it
-- to the unlabeled data.
-- * We assume that the first U elements of the data are unlabeled and the next L elements
-- are labeled.
--
-- Input:
--		- unlabeled_data: tensor with unlabeled features [U x F]
--		- labeled_data: tensor with labeled features [L x F]
--		- label_groups: table with pair <label, indices list w.r.t. data_labels>
--		- k: parameter to select the kth nearest neighbors
--		- data_labels: true labels of all the data [N] -> N = U + L
--
-- Output: 
--		- assignment: table with assigned labels and distances (unlabeled only) [N x 1 x 2]
--					  [i][1][1] = assigned label
--					  [i][1][2] = nearest distance for unlabeled data
--					  [i][1][2] = -1 for labeled data
--
-- U = number of unlabeled samples, L = number of labeled samples, F = number of features
-- N = total number of elements (U+L)
--
local knn = require 'knn' -- library to compute knn with cuda

function fastAssignmentByDistanceSemiSupervised(unlabeled_data, labeled_data, label_groups, k, data_labels)
    local assignment = {}

    -- calculate the k-nn elements using cuda
    local knn_distances, knn_indices = knn.knn(labeled_data:float(), unlabeled_data:float(), k)
    local unlabeled_size = unlabeled_data:size(1)

    -- assign labels to unlabeled data based on k-nn
    for index=1,unlabeled_size do
        if k == 1 then
			-- if nearest neighbor then use direct assignment
            assignment[index] = {}
            assignment[index][1] = {}
            assignment[index][1][1] = data_labels[knn_indices[index][1]] --assign label of correct cluster
            assignment[index][1][2] = knn_distances[index][1]
        else
            -- if knn elements is less than parameter k, use all the knn elements
            local knn = math.min(k, knn_distances[index]:size()[1])
            local countLabel = {}
            local maximum, maximum_label, distance = 0, -1, -1

            -- assign label based on the most common element
            for j=1,knn do
                local label = data_labels[knn_indices[index][j]]
                if countLabel[ label ] == nil then
                    countLabel[ label ] = 0
                end
                countLabel[ label ] = countLabel[ label ] + 1
                if( countLabel[ label ] > maximum ) then
                    maximum = countLabel[ label ]
                    maximum_label = label
                    distance = knn_distances[index][j]
                end
            end

           local correct_index = index
           -- keep the label in position 1 and distance in position 2
           assignment[correct_index] = {}
           assignment[correct_index][1] = {}
           assignment[correct_index][1][1] = maximum_label
           assignment[correct_index][1][2] = distance
        end
    end

    -- for labeled data, assign directly its true label with distance -1
    for i=1,labeled_data:size(1) do
        local correct_index = i + unlabeled_size
        assignment[correct_index] = {}
        assignment[correct_index][1] = {}
        assignment[correct_index][1][1] = data_labels[i]
        assignment[correct_index][1][2] = -1 -- identifier of labeled data
    end
    return assignment
end


-- Get a subset of elements from labeled data given a percentage/quantity
-- 
-- Input:
--		- label_groups: table with pair <label, indices list w.r.t. data_labels>
--		- proportion of the labeled [0,1] - 0.3 means 30% of labeled data or 
--		  number of labeled data per class - 10 means 10 labels per class
--
-- Output: 
--		- data indices with the subset selected
--
function getSubsetFromLabeledGroup(label_groups, proportion)
	local indices = torch.LongTensor()

	-- iterate over each cluster and get a proportion of the data
	for label,indices_list in pairs(label_groups) do
		local num_elements_class = #indices_list

		-- consider all labeled elements if their number is less than proportion
		local to_consider = math.min(num_elements_class, proportion)

		-- consider proportion as percentage
		if( proportion <= 1 ) then
			to_consider = math.floor(num_elements_class * proportion)
		end

		-- randomly select a proportioon of data
		local indices_random = torch.randperm(num_elements_class):long()
		indices_list = torch.LongTensor(indices_list)
		indices = torch.cat( indices, indices_list:index(1,indices_random[{{1,to_consider}}]) )
	end
	return indices
end


-- Calculate doublet values and indices for anchor and positive, indices are based on features
-- matrix anchor and positive belongs to same cluster/group
--  
-- Input:
--		- features: matrix with features for each element [NxD]
--		- assignment: cluster_id assigned to each element of features [N]
--		- label_groups: indices of elements per cluster [KxL] L:#elements per cluster 
--						- cluster[k] = [ index1, index5, index10, ...]
--
-- Output:
--		- {anchor, positive} doublets selected [NDxD]
--		- indices of anchor and positive given, according feature matrix
--
-- ND = num doublets according parameters, N = number of elements, K = number of clusters
--
function calculateDoublet(features, assignment, label_groups)
	local num_samples = features:size(1)
	local num_features = features:size(2)
	local num_doublets = 0

	-- calculate the number of doublets that will be created
	for i=1, num_samples do
		local assignmentId = assignment[i][1][1]
		num_doublets = num_doublets + #label_groups[assignmentId]
	end

	local anchor = torch.Tensor(num_doublets, num_features):typeAs(features)
	local positive = torch.Tensor(num_doublets, num_features):typeAs(features)
	local isLabeled = torch.ShortTensor(num_doublets):zero()

	local anchor_indices = torch.LongTensor(num_doublets):zero()
	local positive_indices = torch.LongTensor(num_doublets):zero()

	-- create doublet indices <anchor, positive> being anchor the current element
	-- and positive, an element that belongs to the same cluster
	-- isLabeled assigns a 1 if the current anchor is labeled
	local doubletId = 1
	for i=1, num_samples do
		local assignmentId = assignment[i][1][1]
		local indices_label = label_groups[assignmentId]
		for j=1, #indices_label do
			anchor_indices[doubletId] = i
			positive_indices[doubletId] = indices_label[j]
			if( assignment[i][1][2] == -1 ) then
				isLabeled[doubletId] = 1
			end
			doubletId = doubletId + 1
		end
	end

	-- use the indices to select the respective features for anchor and positive
	anchor:indexCopy(1, torch.range(1, num_doublets):long(), features:index(1,anchor_indices))
	positive:indexCopy(1, torch.range(1, num_doublets):long(), features:index(1,positive_indices))

	return {anchor, positive, isLabeled}, {anchor_indices, positive_indices}, isLabeled
end


-- Calculate doublet values/indices for anchor and positive (considering only labeled data), 
-- indices are based on features matrix anchor and positive belongs to same cluster/group
-- 
-- Input:
--		- features: matrix with features for each element [NxD]
--		- label_groups: indices of elements per cluster [KxL] L:#elements per cluster 
--						- cluster[k] = [ index1, index5, index10, ...]
--
-- Output:
--		- {anchor, positive} doublets selected [NDxD]
--		- indices of anchor and positive given, according feature matrix
--
-- ND = num doublets according parameters, N = number of elements, K = number of clusters
--
function calculateDoubletLabeled(features, label_groups)
	local num_features = features:size(2)
	local num_doublets = 0
	local num_samples = 0
	
	-- calculate the number of doublets that will be created
	for label, indices_label in pairs(label_groups) do	
		local elements = #indices_label
		num_doublets = num_doublets + elements * (elements - 1)/2
		num_samples = num_samples + elements
	end

	local anchor = torch.Tensor(num_doublets, num_features):typeAs(features)
	local positive = torch.Tensor(num_doublets, num_features):typeAs(features)

	local anchor_indices = torch.LongTensor(num_doublets):zero()
	local positive_indices = torch.LongTensor(num_doublets):zero()	

	-- create doublet indices <anchor, positive> being anchor the current element
	-- and positive, an element that belongs to the same cluster
	local doubletId = 1
	for label, indices_label in pairs(label_groups) do
		local elements = #indices_label
		for i=1,elements do
			local anchor_index = indices_label[i]
			for j=i+1,elements do
				local positive_index = indices_label[j]
				anchor_indices[doubletId] = anchor_index	
				positive_indices[doubletId] = positive_index
				doubletId = doubletId + 1
			end
		end
	end
	
	-- use the indices to select the respective features for anchor and positive
	anchor:indexCopy(1, torch.range(1, num_doublets):long(), features:index(1,anchor_indices))
	positive:indexCopy(1, torch.range(1, num_doublets):long(), features:index(1,positive_indices))

	return {anchor, positive}, {anchor_indices, positive_indices}
end


-- Update the indices after using doublets
-- This is used to get back from doublets to original gradient size
-- 
-- Input:
--		- distances_grad: matrix with the gradients of features for each element [NxF]
--		- doublet_grad: gradient of each doublet obtained after performing backpropagation [ND]
--						we use it after EuclideanEmbedding criterion
--		- doublet_indices: indices of each doublet w.r.t. original size N [ND]
--		- use_all: wheter to use gradients for both anchor and positive or just anchor
--
-- Output:
--		- distances_grad: updated gradient with values of doublets accumulated
--
-- ND = number doublets according parameters, N = number of elements, K = number of clusters
-- F = number of features 
--
function updateIndicesFromDoublets(distances_grad, doublet_grad, doublet_indices, use_all)
	local usingCudaTensor = false

	-- Convert to double because of precision errors with float
	if distances_grad:type() == 'torch.CudaTensor' then
		usingCudaTensor = true
		distances_grad = distances_grad:cudaDouble()
		doublet_grad[1] = doublet_grad[1]:cudaDouble()
		doublet_grad[2] = doublet_grad[2]:cudaDouble()
	end

	-- Accumulate gradients in the given indices
	distances_grad:indexAdd(1, doublet_indices[1], doublet_grad[1])
	if use_all == nil or use_all == true then
		distances_grad:indexAdd(1, doublet_indices[2], doublet_grad[2])
	end

	if usingCudaTensor == true then
		distances_grad = distances_grad:cuda()	
	end

	return distances_grad
end


